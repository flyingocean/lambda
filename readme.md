# Lambda 

## 1.项目介绍
### 集博客、项目启动、项目管理...等功能的Developer Platform

## 2.Java开发小组Simple Guide：

> 开发环境要求：
+ jdk1.8+
+ tomcat9
+ maven3.5.4
+ mysql8

> 开发规范手册：
+ [java小组开发规范.md](https://github.com/hsotech/docs/blob/master/developer-guidelines/java%E5%B0%8F%E7%BB%84%E5%BC%80%E5%8F%91%E8%A7%84%E8%8C%83.md)(前端开发小组适用)
+ [阿里巴巴Java开发手册（详尽版）.pdf](https://github.com/hsotech/docs/blob/master/developer-guidelines/%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4Java%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C%EF%BC%88%E8%AF%A6%E5%B0%BD%E7%89%88%EF%BC%89.pdf)

> git参考文档：
+ [git基础](https://github.com/hsotech/docs/tree/master/git-learn)

> Tips

## 3.前端开发小组Simple Guide：

> 基础知识：
+ html5 
+ css3
+ javascript

> 要求掌握前端框架(optional, incomplete)：
+ bootstrap
+ d3js

> 其他技能：
+ ps

>Tips
+ 评论,博客内容得限字数, 大小参考数据库设计
