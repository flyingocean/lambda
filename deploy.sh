#!/usr/bin/env bash

# 关闭tomcat的函数
killTomcat()
{
pid = `ps -ef | grep tomcat | grep java | awk '{print $2}'`
    echo "tomcat Id list : $pid"
    if ["$pid" = ""]
    then
        echo "no tomcat pid alive"
    else
        kill -9 $pid
    fi
}
cd $PROJ_PATH/lambda

#
mvn clean install

killTomcat

# 删除原有的工程文件
rm -rf $TOMCAT_APP_PATH/webapps/lambda
rm -f $TOMCAT_APP_PATH/webapps/lambda.war
rm -f $TOMCAT_APP_PATH/webapps/lambda.war

# 复制新的工程文件
cp $PROJ_PATH/lambda/target/lambda.war $TOMCAT_APP_PATH/webapps/

cd $TOMCAT_APP_PATH/webapps/
mv lambda.war lambda.war

# 启动Tomcat
cd $TOMCAT_APP_PATH/
sh bin/startup.sh