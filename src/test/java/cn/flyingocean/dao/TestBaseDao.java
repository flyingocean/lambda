package cn.flyingocean.dao;

import org.junit.Test;

public class TestBaseDao {

    /*
     * if 'catch'and 'finally' both have 'return' statement,
     * proved that 'return' in 'catch' body will be ignored
     */
    @Test
    public void testExceptionCatch(){
        System.out.println(testExceptionCatchSubFunction());
    }

    public String testExceptionCatchSubFunction(){
        try{
            Object o = 1/0;
            System.out.println("try...");
            return "try return";
        }catch (Exception e){
            System.out.println("catch...");
            return "catch return";
        }finally {
            System.out.println("finally...");
            return "finally return";
        }
    }
}
