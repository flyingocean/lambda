package cn.flyingocean.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * del()等方法使用前需先判断对象是否存在
 * @param <T>
 */
public class BaseDao<T> {
    @Autowired

    //ToDO:
    public SessionFactory sf;

    /**
     * 开启一个hibernate会话,需要手动关闭
     * @return Session
     * @anthor xstar
     */
    public Session getSession(){
        return sf.openSession();
    }

    /**
     * 依据传入的hql查询对象
     * @param hql 传入hql语句
     * @return T:查询成功 null:查询对象不存在
     * @anthor xstar
     */
    protected T queryOne(String hql){
        Session session = getSession();
        T r = (T) session.createQuery(hql).uniqueResult();

        session.close();
        return r;
    }

    /**
     * 持久化传入的对象(主键匹配)
     * @param thing 要保存的对象
     * @author xstar
     */
    protected void save(T thing){
        Session session = getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.save(thing);

            tx.commit();
            session.flush();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    /**
     * 删除传入的对象(主键匹配)
     * @param thing
     * @author xstar
     */
    protected void del(T thing){
        Session session = getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.delete(thing);

            tx.commit();
            session.flush();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    /**
     * 更新传入的对象(主键匹配)
     * @param thing
     * @author xstar
     */
    protected void update(T thing){
        Session session = getSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(thing);

            tx.commit();
            session.flush();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    /**
     * 依据传入的hql获得查询集
     * @param hql
     * @return
     */
    protected List<T> queryList(String hql){
        Session session = getSession();
        List<T> resultList = session.createQuery(hql).list();

        session.close();
        return resultList;
    }
}
