package cn.flyingocean.entity.pojo;

import java.sql.Timestamp;

public class BlogComment {
    private long id;
    private Timestamp gmtCreate;
    private Timestamp gmtModified;
    private String content;
    private long commenterId;
    private long blogId;
    private Long parentCommentId;

    public BlogComment() {
    }

    public BlogComment(String content, Long commenter_id, Long blogId, Long parent_comment_id) {
        this.content = content;
        this.commenterId = commenter_id;
        this.blogId = blogId;
        this.parentCommentId = parent_comment_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(long commenterId) {
        this.commenterId = commenterId;
    }

    public long getBlogId() {
        return blogId;
    }

    public void setBlogId(long blogId) {
        this.blogId = blogId;
    }

    public Long getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Long parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogComment that = (BlogComment) o;

        if (id != that.id) return false;
        if (commenterId != that.commenterId) return false;
        if (blogId != that.blogId) return false;
        if (gmtCreate != null ? !gmtCreate.equals(that.gmtCreate) : that.gmtCreate != null) return false;
        if (gmtModified != null ? !gmtModified.equals(that.gmtModified) : that.gmtModified != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (parentCommentId != null ? !parentCommentId.equals(that.parentCommentId) : that.parentCommentId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gmtCreate != null ? gmtCreate.hashCode() : 0);
        result = 31 * result + (gmtModified != null ? gmtModified.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (int) (commenterId ^ (commenterId >>> 32));
        result = 31 * result + (int) (blogId ^ (blogId >>> 32));
        result = 31 * result + (parentCommentId != null ? parentCommentId.hashCode() : 0);
        return result;
    }
}
