package cn.flyingocean.entity.pojo;

import java.sql.Timestamp;

public class Follow {
    private long id;
    private Timestamp gmtCreate;
    private Timestamp gmtModified;
    private long followerId;
    private long followingId;

    public Follow(long followingId, long followerId) {
        this.followerId = followerId;
        this.followingId = followingId;
    }

    public Follow() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }

    public long getFollowerId() {
        return followerId;
    }

    public void setFollowerId(long followerId) {
        this.followerId = followerId;
    }

    public long getFollowingId() {
        return followingId;
    }

    public void setFollowingId(long followingId) {
        this.followingId = followingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Follow follow = (Follow) o;

        if (id != follow.id) return false;
        if (followerId != follow.followerId) return false;
        if (followingId != follow.followingId) return false;
        if (gmtCreate != null ? !gmtCreate.equals(follow.gmtCreate) : follow.gmtCreate != null) return false;
        if (gmtModified != null ? !gmtModified.equals(follow.gmtModified) : follow.gmtModified != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gmtCreate != null ? gmtCreate.hashCode() : 0);
        result = 31 * result + (gmtModified != null ? gmtModified.hashCode() : 0);
        result = 31 * result + (int) (followerId ^ (followerId >>> 32));
        result = 31 * result + (int) (followingId ^ (followingId >>> 32));
        return result;
    }
}
