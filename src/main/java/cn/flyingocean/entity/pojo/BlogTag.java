package cn.flyingocean.entity.pojo;

import java.sql.Timestamp;

public class BlogTag {
    private long id;
    private Timestamp gmtCreate;
    private Timestamp gmtModified;
    private String tag;
    private long blogId;

    public BlogTag(String tag, long blog_id) {
        this.tag = tag;
        this.blogId = blog_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public long getBlogId() {
        return blogId;
    }

    public void setBlogId(long blogId) {
        this.blogId = blogId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlogTag blogTag = (BlogTag) o;

        if (id != blogTag.id) return false;
        if (blogId != blogTag.blogId) return false;
        if (gmtCreate != null ? !gmtCreate.equals(blogTag.gmtCreate) : blogTag.gmtCreate != null) return false;
        if (gmtModified != null ? !gmtModified.equals(blogTag.gmtModified) : blogTag.gmtModified != null) return false;
        if (tag != null ? !tag.equals(blogTag.tag) : blogTag.tag != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gmtCreate != null ? gmtCreate.hashCode() : 0);
        result = 31 * result + (gmtModified != null ? gmtModified.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (int) (blogId ^ (blogId >>> 32));
        return result;
    }
}
