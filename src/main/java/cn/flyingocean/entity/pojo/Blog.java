package cn.flyingocean.entity.pojo;

import java.sql.Timestamp;

public class Blog {
    private long id;
    private Timestamp gmtCreate;
    private Timestamp gmtModified;
    private Integer countCollected;
    private Integer countLike;
    private Integer countRead;
    private Integer countComment;
    private Byte isBanComment;
    private Long authorId;
    private String title;
    private String content;

    public Blog(long authorId, String blogTitle, String blogContent) {
        this.authorId = authorId;
        this.title = blogTitle;
        this.content = blogContent;
    }

    public Blog() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Timestamp gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Timestamp getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Timestamp gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getCountCollected() {
        return countCollected;
    }

    public void setCountCollected(Integer countCollected) {
        this.countCollected = countCollected;
    }

    public Integer getCountLike() {
        return countLike;
    }

    public void setCountLike(Integer countLike) {
        this.countLike = countLike;
    }

    public Integer getCountRead() {
        return countRead;
    }

    public void setCountRead(Integer countRead) {
        this.countRead = countRead;
    }

    public Integer getCountComment() {
        return countComment;
    }

    public void setCountComment(Integer countComment) {
        this.countComment = countComment;
    }

    public Byte getIsBanComment() {
        return isBanComment;
    }

    public void setIsBanComment(Byte isBanComment) {
        this.isBanComment = isBanComment;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Blog blog = (Blog) o;

        if (id != blog.id) return false;
        if (gmtCreate != null ? !gmtCreate.equals(blog.gmtCreate) : blog.gmtCreate != null) return false;
        if (gmtModified != null ? !gmtModified.equals(blog.gmtModified) : blog.gmtModified != null) return false;
        if (countCollected != null ? !countCollected.equals(blog.countCollected) : blog.countCollected != null)
            return false;
        if (countLike != null ? !countLike.equals(blog.countLike) : blog.countLike != null) return false;
        if (countRead != null ? !countRead.equals(blog.countRead) : blog.countRead != null) return false;
        if (countComment != null ? !countComment.equals(blog.countComment) : blog.countComment != null) return false;
        if (isBanComment != null ? !isBanComment.equals(blog.isBanComment) : blog.isBanComment != null) return false;
        if (authorId != null ? !authorId.equals(blog.authorId) : blog.authorId != null) return false;
        if (title != null ? !title.equals(blog.title) : blog.title != null) return false;
        return content != null ? content.equals(blog.content) : blog.content == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gmtCreate != null ? gmtCreate.hashCode() : 0);
        result = 31 * result + (gmtModified != null ? gmtModified.hashCode() : 0);
        result = 31 * result + (countCollected != null ? countCollected.hashCode() : 0);
        result = 31 * result + (countLike != null ? countLike.hashCode() : 0);
        result = 31 * result + (countRead != null ? countRead.hashCode() : 0);
        result = 31 * result + (countComment != null ? countComment.hashCode() : 0);
        result = 31 * result + (isBanComment != null ? isBanComment.hashCode() : 0);
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
