package cn.flyingocean.entity.vo;

import java.util.List;

public class VO_simple_blog {
    long blogId;//博客
    long blogContentId;//博客内容
    String blogTitle;//博客标题
    String blogContentHead;//博客开头的一部分内容
    String date;//日期
    List<String> tags;//标签
    long pageView;//阅读量


    public long getBlogId() {
        return blogId;
    }

    public void setBlogId(long blogId) {
        this.blogId = blogId;
    }

    public long getBlogContentId() {
        return blogContentId;
    }

    public void setBlogContentId(long blogContentId) {
        this.blogContentId = blogContentId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getBlogContentHead() {
        return blogContentHead;
    }

    public void setBlogContentHead(String blogContentHead) {
        this.blogContentHead = blogContentHead;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public long getPageView() {
        return pageView;
    }

    public void setPageView(long pageView) {
        this.pageView = pageView;
    }
}
