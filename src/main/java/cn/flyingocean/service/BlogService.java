package cn.flyingocean.service;

import cn.flyingocean.entity.pojo.*;
import cn.flyingocean.entity.vo.VO;
import cn.flyingocean.entity.vo.VO_simple_blog;
import cn.flyingocean.entity.vo.VO_user_center;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("BlogService")
public class BlogService {
    public void updateBlog(Blog blog) {
    }

    public Blog queryBlog(long blogId) {
        return null;
    }

    public void saveBlogTag(String tag) {
    }

    public void removeALLTagsOfBlog(long blogId) {
    }

    public long saveBlog(Blog blog) {
        return 0;
    }

    public long saveBlogContent(String blogContent) {
        return 0;
    }

    public void removeBlogContent(long contentId) {
    }

    public void removeBlog(long id) {
    }

    public void saveComment(BlogComment comment) {
    }

    public void loadVO(VO vo) {
        String clazzSimpleName = vo.getClass().getSimpleName();
        //通过判断类名来确定具体的装载方法 (具体的装载方法定义private)
        if ("VO_user_center".equals(clazzSimpleName)){
            loadVO_user_center((VO_user_center)vo);
        }
    }
    private void loadVO_user_center(VO_user_center vo_user_center){

    }

    public void saveFollow(Follow follow) {
    }

    public void removeFollow(long followingId, long followerId) {
    }

    public List<BlogComment> queryAllCommentsOfBlog(long blogId) {
    }

    public List<BlogComment> queryAllCommentsOfComment(long commentId) {
    }

    public int saveBlogCollection(BlogCollection blogCollection) {
    }

    public int removeBlogCollection(long blogId, long userId) {
    }

    public int[] removeBlogCollections(long userId, long[] blogIds) {
    }

    public List<VO_simple_blog> queryBlogWithConditions(String index, long queryedUserId, String sortMethod, String filter, byte pageSize, byte pageNum) {
    }

    public void plusOneComment4Blog(Long blogId) {
    }

    public void minusOneCollection4Blog(long blogId) {
    }

    public int saveBlogLikedMapping(long blogId, long userId) {
    }

    public void plusOneLike4Blog(long blogId) {
    }

    public int removeBlogLikedMapping(long blogId, long userId) {
    }

    public void minusOneLike4Blog(long blogId) {
    }

    public int saveCommentLikedMapping(long commentId, long userId) {
    }

    public int removeCommentLikedMapping(long commentId, long userId) {
    }
}
