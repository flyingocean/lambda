package cn.flyingocean.controller;

import cn.flyingocean.entity.pojo.*;
import cn.flyingocean.entity.vo.*;
import cn.flyingocean.service.BlogService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/blog/")
public class BlogController {
    @Resource(name = "BlogService")
    private BlogService blogService;


    /******************************************************/
    /**********************返回页面*************************/
    /******************************************************/


    //返回广场页面
    @RequestMapping("")
    public String square_page(Model model,HttpSession session){

        long userId = (long) session.getAttribute("userId");
        model.addAttribute("userId",userId);
        //装载vo
        VO_blog_square vo_blog_square = new VO_blog_square();
        blogService.loadVO(vo_blog_square);
        //传递vo
        model.addAttribute("vo_blog_square",vo_blog_square);
        return "square.jsp";
    }

    //返回用户中心页面
    @RequestMapping("user-center/{userId}")
    public String userCenter_page(Model model){
        VO_user_center vo_user_center = new VO_user_center();
        //装载vo
        blogService.loadVO(vo_user_center);
        //传递vo
        model.addAttribute("vo_user_center",vo_user_center);
        return "userCenter.jsp";
    }

    //返回写博客页面
    @RequestMapping("writeBlog")
    public String writeBlog_page(Model model,HttpSession session){
        //参数准备
        long userId = (long) session.getAttribute("userId");

        VO_writeBlog vo_writeBlog = new VO_writeBlog();

        //装载vo
        blogService.loadVO(vo_writeBlog);
        //传递vo
        model.addAttribute("vo_writeBlog",vo_writeBlog);
        //user等信息就不包含在vo里了
        model.addAttribute("",);

        return null;
    }

    /******************************************************/
    /**********************查询请求*************************/
    /******************************************************/


    //处理查询博客请求
    @RequestMapping("doSearch")
    public List<VO_simple_blog> doSearch(@RequestBody Map<String,?> data)
    {
        //参数准备
        long queryedUserId = (Long) data.get("queryedUserId");//所要查询的用户
        String index = (String) data.get("index");//查询索引;
        String sortMethod = (String)data.get("sortMethod"); //排序方式(value: 'Date-Asc'/'Date-Desc'/'PV-Asc'/'PV-Desc')
        String filter = (String)data.get("filter");//筛选条件(value:'Only-Origin'/'Only-Reprint')
        byte pageSize = (Byte)data.get("pageSize");//页长:一页含多少个博客
        byte pageNum = (Byte)data.get("pageNum");//当前页号

        return blogService.queryBlogWithConditions(index,queryedUserId,sortMethod,filter,pageSize,pageNum);
    }

    //处理查询博客一级评论请求
    @RequestMapping("doQueryAllCommentsOfBlog")
    public List<BlogComment> doQueryAllCommentsOfBlog(@RequestBody Map<String,Long> data){
        //参数准备
        long blogId = data.get("blogId");
        return blogService.queryAllCommentsOfBlog(blogId);
    }

    //处理查询博客二级评论
    public List<BlogComment> doQueryAllCommentsOfComment(@RequestBody Map<String,Long>data){
        //参数准备
        long commentId = data.get("commentId");
        return blogService.queryAllCommentsOfComment(commentId);
    }




    /******************************************************/
    /**********************插入请求*************************/
    /******************************************************/

    //处理提交博客请求(只接受post request)
    @RequestMapping(value = "doSubmitBlog",method = RequestMethod.POST)
    @ResponseBody
    public int doSubmitBlog(@RequestBody Map<String,?>data, HttpSession session){
        //参数准备
        String blogContent = (String) data.get("blogContent");
        String blogTitle = (String) data.get("blogTitle");
        List<String> tags = (List<String>) data.get("tags");
        Byte is_ban_comment = (Byte)data.get("is_ban_comment");

        long userId = (long) session.getAttribute("userId");

        //test--0
        System.out.println("blogTitle: "+blogTitle);
        System.out.println("blogContent: "+blogContent);
        for (String tag:tags){
            System.out.print("tags: "+tag+" ");
        }
        //test--1

        //持久化Blog,返回刚插入的blog_id(auto_increment)
        Blog blog = new Blog(userId,blogTitle,blogContent);

        long blog_id = blogService.saveBlog(blog);

        //持久化BlogTag
        for (String tag:tags){
            BlogTag t = new BlogTag(tag,blog_id);
            blogService.saveBlogTag(tag);
        }
        return RETURN.OK;
    }

    //处理提交评论请求
    @RequestMapping(value = "doSubmitComment",method = RequestMethod.POST)
    @ResponseBody
    public int doSubmitComment(@RequestBody Map<String,?>data, HttpSession session){
        //参数准备
        Long blogId = (Long) data.get("blogId");
        Long parent_comment_id = (Long) data.get("parent_comment_id");
        Long commenter_id = (Long) session.getAttribute("userId");
        String content = (String) data.get("content");

        BlogComment comment = new BlogComment(content,commenter_id,blogId,parent_comment_id);

        //持久化评论体
        blogService.saveComment(comment);
        //更新blog信息, 博客信息中评论数加一
        blogService.plusOneComment4Blog(blogId);

        return RETURN.OK;
    }

    //处理关注或取消关注用户请求
    @RequestMapping("doFollowUser")
    @ResponseBody
    public int doFollowUser(@RequestBody Map<String,Long> data,HttpSession session){
        //参数准备
        long followingId = (long) session.getAttribute("userId");//当前登录的用户id(即关注者)
        long followerId = data.get("followerId");//被关注的用户id
        long operationKind = data.get("operationKind");

        if (operationKind==0){//取消关注操作
            //持久化操作
            Follow follow = new Follow(followingId,followerId);
            blogService.saveFollow(follow);
        }else {
            //持久化操作
            blogService.removeFollow(followingId,followerId);
        }

        return RETURN.OK;
    }

    //处理收藏或取消收藏单个博客文章请求
    @RequestMapping("doCollectBlog")
    @ResponseBody
    public int doCollectBlog(@RequestBody Map<String,Long> data,HttpSession session){
        //参数准备
        long userId = (long) session.getAttribute("userId");
        long blogId = data.get("blogId");//收藏的博客id
        long operationKind = data.get("operationKind");//操作类型:是收藏还是取消收藏


        if (operationKind==1){
            //持久化操作
            BlogCollection blogCollection = new BlogCollection();
            //更新博客信息,博客收藏数加一
            return blogService.saveBlogCollection(blogCollection);
        }else {
            //持久化操作
             int r = blogService.removeBlogCollection(blogId,userId);
            //更新博客信息,博客收藏数减一
             if (r==RETURN.OK){
                 blogService.minusOneCollection4Blog(blogId);
                 return r;
             }
             return r;
        }
    }

    //处理批量取消收藏博客文章请求
    @RequestMapping("doCollectBlog")
    @ResponseBody
    public int[] doCollectBlogs(@RequestBody Map<String,?> data,HttpSession session){
        //参数准备
        long userId = (long) session.getAttribute("userId");
        long[] blogIds = (long[])data.get("blogIds");

        int[] r = blogService.removeBlogCollections(userId,blogIds);
        //博客信息更新, 被取消收藏的博客的收藏数都减一
        for (int i = 0;i<r.length;i++){
            if (r[i]==RETURN.OK){
                blogService.minusOneCollection4Blog(blogIds[i]);
            }
            return r;
        }
        return r;
    }


    /******************************************************/
    /**********************更新请求*************************/
    /******************************************************/


    //处理更新博客请求
    @RequestMapping(value = "doUpdateBlog",method = RequestMethod.POST)
    @ResponseBody
    public int doUpdateBlog(@RequestBody Map<String,?>data, HttpSession session){
        //参数准备
        long blogId =  (Long) data.get("blogId");
        String blogContent = (String) data.get("blogContent");
        String blogTitle = (String) data.get("blogTitle");
        List<String> tags = (List<String>) data.get("tags");
        byte is_ban_comment = (Byte) data.get("is_ban_comment");

        long userId = (Long) session.getAttribute("userId");

        //test--0
        System.out.println("blogTitle: "+blogTitle);
        System.out.println("blogContent: "+blogContent);
        for (String tag:tags){
            System.out.print("tags: "+tag+" ");
        }
        //test--1
       //根据传入的blogId获取Blog
        Blog blog = blogService.queryBlog(blogId);
        if (blog==null) {//如果不存在则返回状态码
            return RETURN.NOT_ACCEPTABLE;
        }
        //用户认证
        if (blog.getAuthorId()!=userId){
            return RETURN.UNAUTHORIZED;
        }
        //持久化blog的更新
        blog.setIsBanComment(is_ban_comment);
        blog.setTitle(blogTitle);
        blog.setContent(blogContent);
        blogService.updateBlog(blog);

        //删除原本所有的tag
        blogService.removeALLTagsOfBlog(blogId);
        //持久化BlogTag
        for (String tag:tags){
            BlogTag t = new BlogTag(tag,blogId);
            blogService.saveBlogTag(tag);
        }
        return RETURN.OK;
    }

    //处理锁定或解锁博客评论请求
    @RequestMapping(value = "doUpdateBlogIsBanComment",method = RequestMethod.POST)
    @ResponseBody
    public int doUpdateBlogIsBanComment(@RequestBody Map<String,?>data,HttpSession session){
        //参数准备
        long blogId = (Long) data.get("blogId");
        long userId = (Long) session.getAttribute("userId");
        byte is_ban_comment = (Byte) data.get("is_ban_comment");
        Blog blog = blogService.queryBlog(blogId);

        //用户认证
        if (blog.getAuthorId()!=userId){
            return RETURN.UNAUTHORIZED;
        }

        blog.setIsBanComment(is_ban_comment);
        //持久化修改
        blogService.updateBlog(blog);
        return RETURN.OK;
    }


    //处理点赞博客
    @RequestMapping("doLikeBlog/{operationKind}/{blogId}")
    @ResponseBody
    public int doLikeBlog(@PathVariable byte operationKind, @PathVariable long blogId,HttpSession session){
        //参数准备
        long userId = (long) session.getAttribute("userId");

        if (operationKind==1){
            //持久化一条用户喜欢博客的映射关系
            int r = blogService.saveBlogLikedMapping(blogId,userId);

            if (r==RETURN.OK){
                //更新博客信息, 博客点赞数加一
                blogService.plusOneLike4Blog(blogId);
            }
            return r;
        }else{
            //移除一条用户喜欢博客的映射关系
            int r = blogService.removeBlogLikedMapping(blogId,userId);

            if (r==RETURN.OK){
                //更新博客信息, 博客点赞数加一
                blogService.minusOneLike4Blog(blogId);
            }
            return r;
        }
    }

    //处理点赞评论
    @RequestMapping("doLikeBlog/{operationKind}/{blogId}")
    @ResponseBody
    public int doLikeComment(@PathVariable byte operationKind, @PathVariable long commentId,HttpSession session){
        //参数准备
        long userId = (long) session.getAttribute("userId");

        if (operationKind==1){
            //持久化一条用户点赞评论的映射关系
            return blogService.saveCommentLikedMapping(commentId,userId);

        }else{
            //移除一条用户点赞评论的映射关系
           return blogService.removeCommentLikedMapping(commentId,userId);
        }
    }

    /******************************************************/
    /**********************删除请求*************************/
    /******************************************************/


    //处理删除博客请求
    @RequestMapping(value = "doDeleteBlog",method = RequestMethod.POST)
    @ResponseBody
    public int doDeleteBlog(@RequestBody Map<String,Long>data,HttpSession session){
        //参数准备
        long blogId = data.get("blogId");

        long userId = (long) session.getAttribute("userId");

        Blog blog = blogService.queryBlog(blogId);
        if (blog==null){//如果该博客文章不存在
            return RETURN.NOTFOUND;
        }
        //用户认证
        if (userId!=blog.getAuthorId()){
            return RETURN.UNAUTHORIZED;
        }
        //删除博客所有的tag
        blogService.removeALLTagsOfBlog(blogId);
        //删除博客体
        blogService.removeBlog(blog.getId());

        return RETURN.OK;
    }


}
