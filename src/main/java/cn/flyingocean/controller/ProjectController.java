package cn.flyingocean.controller;

import cn.flyingocean.service.ProjectService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/project/")
public class ProjectController {
    @Resource(name = "ProjectService")
    private ProjectService projectService;
}
