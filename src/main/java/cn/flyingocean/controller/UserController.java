package cn.flyingocean.controller;

import cn.flyingocean.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/user/")
public class UserController {
    @Resource(name = "UserService")
    private UserService userService;

    /******************************************************/
    /**********************返回页面*************************/
    /******************************************************/

    //返回登录页面
    @RequestMapping("signIn")
    public String signInPage() {
        return "/user/signIn.jsp";
    }


    //返回忘记密码页面
    @RequestMapping("forgetPWD")
    public String forgetPWD() {
        return "/user/forgetPWD.jsp";
    }


    /******************************************************/
    /**********************查询请求*************************/
    /******************************************************/

    //处理登录请求
    @RequestMapping("doSignIn")
    @ResponseBody
    public int doSignIn(@RequestBody Map<String,String> data) {
        //参数准备
        String email =  data.get("email");
        String password =  data.get("password");
        String verification_code = (String)data.get("verification-code");

        //ToDo: 完成验证码验证
        //...

        return userService.signIn(email,password);
    }

    /******************************************************/
    /**********************插入请求*************************/
    /******************************************************/

    @RequestMapping("doSignUp")
    @ResponseBody
    public int doSignUp(@RequestBody Map<String,String> data){
        //参数准备
        String email = data.get("email");
        String password = data.get("password");
        String verification_code = data.get("verification-code");

        //ToDo: 完成验证码验证
        //...

        return userService.signUp(email,password);
    }

}
