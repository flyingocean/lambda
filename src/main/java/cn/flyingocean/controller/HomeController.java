package cn.flyingocean.controller;

import cn.flyingocean.service.HomeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/home/")
public class HomeController {
    @Resource(name = "HomeService")
    private HomeService homeService;
}
