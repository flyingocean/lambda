package cn.flyingocean.controller;

import cn.flyingocean.service.ActivityService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/activity/")
public class ActivityController {
    @Resource(name = "ActivityService")
    private ActivityService activityService;
}
