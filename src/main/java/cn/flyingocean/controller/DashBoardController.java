package cn.flyingocean.controller;

import cn.flyingocean.service.DashBoardService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/dashboard/")
public class DashBoardController {
    @Resource(name = "DashBoardService")
    private DashBoardService dashBoardService;
}
