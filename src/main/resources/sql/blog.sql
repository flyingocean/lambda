create database lambda;
use lambda;
create table `user`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `nickname` varchar(20) not null,
    `password` varchar(20) not null,
    `email` varchar(20) not null,
    `avatar` varchar(50) default null,
    `gender` tinyint unsigned default null,
    `birthday` date default null,
    `homeland` varchar(50) default null,
    `introduce` varchar(100) default null,
    constraint `pk_id` primary key (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blog`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `count_collected` int unsigned default 0,
    `count_like` int unsigned default 0,
    `count_read` int unsigned default 0,
    `count_comment` int unsigned default 0,
    `is_ban_comment` tinyint unsigned default 0 comment '是否禁止评论，默认0，表示可以评论',
    `author_id` BIGINT UNSIGNED COMMENT '作者id',
    `title` VARCHAR(50) NOT NULL,
    `content` text not null,
    constraint `fk_blog_Ref_user_id` foreign key (`user_id`) references `user` (`id`),
    constraint `pk_id` primary key (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blog_tag`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `tag` varchar(20) not null,
    `blog_id` bigint unsigned not null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_tag_Ref_blog_id` foreign key (`blog_id`) references `blog` (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blog_collection`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `blog_id` bigint unsigned not null,
    `collector_id` bigint unsigned not null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_blog_collection_Ref_blog_id` foreign key (`blog_id`) references `blog` (`id`),
    constraint `fk_blog_collection_Ref_user_id` foreign key (`collector_id`) references `user` (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blog_comment`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `content` varchar(100) not null,
    `commenter_id` bigint unsigned not null comment '评论者',
    `blog_id` bigint unsigned not null,
    `parent_comment_id` bigint unsigned default null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_blog_comment_Ref_user_id` foreign key (`commenter_id`) references `user` (`id`),
    constraint `fk_blog_comment_Ref_blog_id` foreign key (`blog_id`) references `blog` (`id`),
    constraint `fk_comment_Ref_blog_comment_id` foreign key (`parent_comment_id`) references blog_comment (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `follow`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `follower_id` bigint unsigned not null,
    `following_id` bigint unsigned not null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_follow_follower_Ref_user_id` foreign key (`follower_id`) references `user` (`id`),
    constraint `fk_follow_following_Ref_user_id` foreign key (`following_id`) references `user` (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blogLikedMapping`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `user_id` bigint unsigned not null,
    `blog_id` bigint unsigned not null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_blogLikedMapping_Ref_user_id` foreign key (`user_id`) references `user` (`id`),
    constraint `fk_blogLikedMapping_Ref_blog_id` foreign key (`blog_id`) references `blog` (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;

create table `blogCommentLikedMapping`(
    `id` bigint unsigned not null auto_increment,
    `gmt_create` datetime not null default now(),
    `gmt_modified` datetime not null default now() on update now(),
    `user_id` bigint unsigned not null,
    `comment_id` bigint unsigned not null,
    constraint `pk_id` primary key (`id`),
    constraint `fk_blogComment_Ref_user_id` foreign key (`user_id`) references `user` (`id`),
    constraint `fk_blogCommentLikedMapping_Ref_blog_id` foreign key (`comment_id`) references `blog_comment` (`id`)
) engine = innodb auto_increment = 1 character set = utf8mb4;